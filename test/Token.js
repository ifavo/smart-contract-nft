const Contract = artifacts.require('NFT')

// improve testing using https://github.com/AnAllergyToAnalogy/ERC721/blob/master/tests/Token.test.js

contract('NFT', accounts => {
  const [firstAccount, secondAccount] = accounts

  describe('VIP 181', () => {
    it('supports name', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      const name = await contractInstance.name.call()
      expect(name).to.equal('NFT Token Name')
    })

    it('supports symbol', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      const symbol = await contractInstance.symbol.call()
      expect(symbol).to.equal('NFT')
    })

    it('supports totalSupply', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      await contractInstance.createToken(firstAccount, "hash0", "metadata")
      await contractInstance.createToken(firstAccount, "hash1", "metadata")
      const totalSupply = await contractInstance.totalSupply.call()

      expect(totalSupply).to.deep.equal(web3.utils.toBN(2))
    })

    it('supports transferFrom(from, to, tokenId)', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash0", "metadata", { from: firstAccount })
      await contractInstance.transferFrom(firstAccount, secondAccount, tokenId)

      const owner = await contractInstance.ownerOf.call(tokenId)
      expect(owner).to.equal(secondAccount)
    })

    it('supports balanceOf(address)', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash0", "metadata", { from: firstAccount })

      const balance = await contractInstance.balanceOf.call(firstAccount)
      expect(balance).to.deep.equal(web3.utils.toBN(1))
    })

    it('supports tokenOfOwnerByIndex(address, index)', async () => {
      const contractInstance = await Contract.new("NFT Token Name", "NFT")
      const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash0", "metadata", { from: firstAccount })

      const tokenAtIndex0 = await contractInstance.tokenOfOwnerByIndex.call(firstAccount, 0)
      expect(tokenAtIndex0).to.deep.equal(tokenId)
    })
    

    describe('Token', () => {
      it('supports ownerOf(tokenId)', async () => {
        const contractInstance = await Contract.new("Test", "TTT")
        const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash2", "metadata")
        const owner = await contractInstance.ownerOf.call(tokenId)
        expect(owner).to.equal(firstAccount)
      })

      it('supports tokenURI(tokenId)', async () => {
        const tokenURI = "http://uri"
        const contractInstance = await Contract.new("Test", "TTT")
        const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash2", tokenURI)
        const result = await contractInstance.tokenURI.call(tokenId)
        expect(result).to.equal(tokenURI)
      })

      it('supports tokenByIndex()', async () => {
        const contractInstance = await Contract.new("NFT Token Name", "NFT")
        const { logs: [{ args: { tokenId } }] } = await contractInstance.createToken(firstAccount, "hash0", "metadata")
        const tokenAtIndex0 = await contractInstance.tokenByIndex.call(0)

        expect(tokenAtIndex0).to.deep.equal(tokenId)
      })
    })
  })
})