## Setup
```shell
yarn add @openzeppelin/contracts
npx truffle console --network testnet
```

## Test

```shell
yarn test

```

## Deploy

```shell
yarn deploy
```

## Interact

```shell
$ npx truffle console --network testnet        
truffle(testnet)> const contract = await NFT.deployed()
undefined
truffle(testnet)> await contract.createToken("0x8ca0Ed6e92395055c912f1d8af3004b2A59E3fe1", "hash", "https://favo.org")
{
  tx: '0x789743f4c3861ebd86adcd27bc6a95f202fa77590fc565a1d8daf941f1fa7d84',
  receipt: {
    status: true,
    transactionHash: '0x789743f4c3861ebd86adcd27bc6a95f202fa77590fc565a1d8daf941f1fa7d84',
    transactionIndex: 0,
    blockNumber: 8631710,
    blockHash: '0x0083b59ebd4f913ddb123dc9d735a767ba29d83a5aed6ebba8ecfd36d782a829',
    cumulativeGasUsed: 234181,
    gasUsed: 234181,
    contractAddress: null,
    logs: [ [Object] ],
    rawLogs: [ [Object] ]
  },
  logs: [
    {
      type: 'mined',
      logIndex: 0,
      transactionIndex: 0,
      transactionHash: '0x789743f4c3861ebd86adcd27bc6a95f202fa77590fc565a1d8daf941f1fa7d84',
      blockHash: '0x0083b59ebd4f913ddb123dc9d735a767ba29d83a5aed6ebba8ecfd36d782a829',
      blockNumber: 8631710,
      address: '0x8ca0Ed6e92395055c912f1d8af3004b2A59E3fe1',
      id: 'log_eceef2e0',
      event: 'Transfer',
      args: [Result]
    }
  ]
}
truffle(testnet)> await contract.name()
'My NFT Contract'
truffle(testnet)> await contract.symbol()
'NFT'
truffle(testnet)> await contract.tokenURI(1)
'https://favo.org'
```